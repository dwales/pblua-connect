#README

This is a simple script which makes connecting to an NXT robot running pbLua firmware incredibly easy.

Note: This code was written on a Mac, and assumes that the NXT will appear as `/dev/tty.usbmodem*`
This may break on other systems, or even if you plug multiple NXTs or other USB devices into a single Mac.

If you get it working on Linux, send me a pull request. (I imagine we just need to find out what Linux calls
`/dev/tty.usbmodem*`, and differentiate with `uname`)

#USAGE

Copy the script to wherever you like (possibly on your `$PATH`).
Run the script.

    ./nxt   # if you didn't put it in your $PATH
    nxt     # if you did put it in your $PATH

I call the script `nxt`, and have installed it to `~/bin`, which is on my `$PATH`.
You can call it whatever you like, and put it wherever you like.
